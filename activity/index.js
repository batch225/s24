// Activity Codes

const getCube = Math.pow(2,3);
console.log(`The cube of 2 is ${getCube}`);

const fullAddress = ["258 Washington Ave NW", "California", "90011"];
const [lineOne, state, zipCode] = fullAddress
console.log(`I live at ${lineOne}, ${state} ${zipCode}`);


const animal = {
    name: "Lolong",
    weight: "1075 kgs",
    length: "20 ft 3 in"
}

const {name, weight, length} = animal;

console.log(`${name} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${length}`);


const numbers = [1, 2, 3, 4, 5, 15]
numbers.forEach((number) => {
	console.log(number);
});

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);


// Sir CJ Answer:

// Exponent Operator
const getCube = 2 ** 3;

// Template Literals
console.log(`The cube of 2 is ${getCube}`);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [ houseNumber, street, state, zipCode ] = address;

console.log(`I live at ${houseNumber}, ${street}, ${state} ${zipCode}`);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const { name, species, weight, measurement } = animal;

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

// numbers.forEach((item) => console.log(`${item}`));


// let Addnum = 0;

// numbers.forEach((item) => Addnum += item);
// console.log(Addnum);



numbers.forEach((number) => console.log(number));

let reduceNumber = numbers.reduce((x, y) => x + y);
// (Accumulator , CurrentValue);


// 0 + 1 = 1 - accumulator
// 1 + 2 = 3 - new accumulator
// 3 + 3 = 6
// 6 + 4 = 10
// 10 + 5 = 15

console.log(reduceNumber);


// Javascript Classes
class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
		
	}
}

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");

console.log(myDog);

// // ES5
// function Laptop(name, manufactureDate){
//     this.name = name;
//     this.manufactureDate = manufactureDate;

//     // return {name: name, manufactureDate: manufactureDate};
// }

// let laptop = new Laptop('Lenovo', 2008);
// console.log('Result from creating objects using object constructors:');
// console.log(laptop);

