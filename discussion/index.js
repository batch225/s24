// console.log("Hiloo");

// JAVASCRIPT ES6 Updates

// [SECTION] Exponent Operator

// ES5 Version
const firstNum = 8 ** 2;
console.log(firstNum);

// ES^ Update
const secondNum = Math.pow(8,2);
console.log(secondNum);

// [SECTION] Template Literals

/*

	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability.

*/

let name = "John";

// pre-Template literals string
// ES5 version

let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:" + message);

// String using Template literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`
console.log(message);

// Multi-line using Template Literals

const anotherMessage = `${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`

console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal} * ${interestRate}`);

// [SECTION] Array Destructuring 

/*

 - Allows to unpack elements in arrays into distinct variable
 - Allows us to name array elements with variables instead of using index numbers
 	-helps with code readability
 	-Syntax
		let/const [variableName1, variableName2, variableName3] = arrayName

*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-Array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

// Array Destructuring

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`);

// [SECTION] Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;

*/

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// Object Destructuring
const { givenName, maidenName, familyName } = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({givenName, maidenName, familyName}) {
    console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

// [SECTION] Arrow Function

/*

	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code.
	-"DRY" (Don't Repeat Yourself) principle where there's mno longer need to create a function and think of a name for functions that will only be used in certain code snippets.

	- Syntax

	const variableName = () => {
		console.log();
	}
*/

const hello = () => {
	console.log("Hello World");
};

hello();

// ES5 Functions

function printFullName (firstName, middleInitial, lastName) {
    console.log(firstName + ' ' + middleInitial + '. ' + lastName);
};

printFullName("John", "D", "Smith");

// ES6 Arrow functions

const printName = (first, middle, last) => {
	console.log(`${first} ${middle}. ${last}`)
};

printName("John", "D", "George");

// Arrow Functions with loops
// Pre-Arrow Function

const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
	console.log(`${student} is a student.`);
});

// Arrow Function
// The function is only used in the "forEach" method to print out a text with the student's names
students.forEach((student) => {
	console.log(`${student} is a student.`);
});

// [SECTION] Implicit Return Stament
/*
	- There are instances when you omit the "return" statement
	- This works because even without the "return" JavaScript implicitly adds it for the result of the function.

	Syntax:
	// Pre-Arrow Function 

	const add = (x, y) => {
	return x + y;
	}
*/

const add = (x, y) => x + y;

let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided when the function is invoked.

const greet = (name = "Username") => {
	return `Good Morning, ${name}!`;
}

console.log(greet());


// [SECTION] Class-Based Object Blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class
/*
	- The constructor is a special method of a class for creating/initializing an object for that class.
	- The "this" keyword refers to the properties of an object created/initialized from the class
	- By using the "this" keyword and accessing an object's property, this allows us to reassign it's values
	- Syntax
		class className {
			constructor(objectPropertyA, objectPropertyB) {
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

/*
	ES5 
	function Pokemon(name,level) {

		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level;
		}
*/


class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiating an object
/*
	- The "new" operator creates/instantiates a new object with the given arguments as the values of it's properties
	- No arguments provided will create an object without any values assigned to it's properties
	- let/const variableName = new ClassName();
*/
// let myCar = new Car();

/*
	- Creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't re-assign it with another data type
	- It does not mean that it's properties cannot be changed/immutable
*/
const myCar = new Car();

console.log(myCar);

// Values of properties may be assigned after creation/instantiation of an object
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

// Creating/instantiating a new object from the car class with initialized values

const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);


















